jQuery(document).ready(function ($) {

    $('.article-share > span.fa').on('click', function (event) {
        $(this).parent('.article-share').addClass('share-active');
        event.stopPropagation();
    });

    $(window).on('click', function (event) {
        $('.article-share').removeClass('share-active');
    });

    var winWidth = $(window).width();
    // if (winWidth <= 800) {
    $('.mobile-responsive .mobile-menu-wrap').hide();
    $('.mobile-responsive .toggle-button').on( 'click', function () {
        $('.mobile-responsive .mobile-menu-wrap').slideToggle();
        $('.toggle-button').css('display', 'none');
        $('.close.close-main-nav-toggle').css('display', 'block');
    });

    $('.main-navigation .toggle-button').on('click', function () {
        $('.site').removeClass('toggle-active');
    });

    $('.close.close-main-nav-toggle').on('click', function () {
        $('.mobile-responsive .mobile-menu-wrap').slideToggle();
        $('.site').removeClass('toggle-active');
        $('.toggle-button').css('display', 'block');
        $('.close.close-main-nav-toggle').css('display', 'none');
    });

    $('<button class="submenu-toggle"><i class="fas fa-angle-down"></i></button>').insertAfter($('.main-navigation.mobile-navigation ul .menu-item-has-children > a'));
    $('.main-navigation.mobile-navigation ul li .submenu-toggle').on('click', function () {
        $(this).next().slideToggle();
        $(this).toggleClass('active');
    });
    // }

    // header-search-form
    $('html').on( 'click', function () {
        $('.site-header .header-t-search .form-holder ').hide();
    });

    $('.site-header .header-t-search').on( 'click', function (event) {
        event.stopPropagation();
    });
    $(".site-header .search-btn").on( 'click', function () {
        $(".site-header .header-t-search .form-holder ").slideToggle();
        return false;
    });

    $(".btn-form-close").on( 'click', function () {
        $(".site-header .header-t-search .form-holder ").slideToggle();
        return false;
    });

    // toggle sidebar 
    $('.sidebar-toggleButton').on( 'click', function () {
        $('.site-header .sidebar-wrap').toggleClass('active');
        $('.site').toggleClass('sidebar-active');
    });

    $('.site-header .sidebar-wrap .toggle-button').on( 'click', function () {
        $('.site-header .sidebar-wrap.active').removeClass('active');
        $('.site').removeClass('sidebar-active');
    });

    $('.overlay').on( 'click', function () {
        $('.site-header .sidebar-wrap.active').removeClass('active');
        $('.site').removeClass('sidebar-active');
    });

    // Custom scrollbar
    if (rara_readable_data.sidebar_active) {
        new PerfectScrollbar('.sidebar-wrap .sidebar');
    }

    //for menu drop down in edge
    if (winWidth > 800) {
        $("#primary-navigation ul li a").on( 'focus', function () {
            $(this).parents("li").addClass("focus");
        }).on( 'blur', function () {
            $(this).parents("li").removeClass("focus");
        });
    }

}); //document close
